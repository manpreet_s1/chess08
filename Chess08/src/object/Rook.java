package object;

import board.Board;
/**
 * 
 * @author Manpreet Singh
 * @author Daniel Egladyous
 */


public class Rook extends Piece {
	/**
	 *  Arg Constructor for a Rook
	 * @param x Current X coordinate
	 * @param y Current Y coordinate
	 * @param team True is White, False is Black
	 */

	public Rook(int x, int y, boolean team) {
		this.x = x;
		this.y = y;
		
		this.team = team;
	}
	/**
	 * A check to see if a move is possible
	 * @param board The board where the piece is on
	 * @param oldx The old x coordinate value
	 * @param oldy The old x coordinate value
	 * @param x The new x coordinate value
	 * @param y The new y coordinate value
	 * @param dir The direction in which the piece wants to move
	 * @return True if move is possible
	 */
	
	private boolean lineCheck(Piece[][] board, int oldx, int oldy, int x, int y, int dir) {
		// 5 (east) 6 (west) 7 (north) 8 (south)
		if (dir == 5) {
			while (oldy != (y-1)) {
				oldy++;
				if (!Board.isEmpty(board, oldx, oldy)) {
					return false;
				}
			}
		} else if (dir == 6) {
			while (oldy != (y+1)) {
				oldy--;
				if (!Board.isEmpty(board, oldx, oldy)) {
					return false;
				}
			}
		} else if (dir == 7) {
			while (oldx != (x+1)) {
				oldx--;
				if (!Board.isEmpty(board, oldx, oldy)) {
					return false;
				}
			}
		} else if (dir == 8) {
			while (oldx != (x-1)) {
				oldx++;
				if (!Board.isEmpty(board, oldx, oldy)) {
					return false;
				}
			}
		} else {
			return false;
		}
		
		return true;
	}
	

	@Override
	public boolean isPathClear(Piece[][] board, int x, int y) {
		
		int oldx = this.x;
		int oldy = this.y;
		
		int deltax = Math.abs(x - oldx);
		int deltay = Math.abs(y - oldy);
		
		int dx = oldx - x;
		int dy = oldy - y;
		
		if (deltax == 0 && deltay != 0) { // horizontal
			if (dy < 0) { // moving right
				return lineCheck(board, oldx, oldy, x, y, 5);
			} else { // moving left
				return lineCheck(board, oldx, oldy, x, y, 6);
			}
		} else if (deltax != 0 && deltay == 0) { // vertical
			if (dx > 0) { // moving up
				return lineCheck(board, oldx, oldy, x, y, 7);
			} else { // moving down
				return lineCheck(board, oldx, oldy, x, y, 8);
			}
		}
		
		return false;
	}
}
	

