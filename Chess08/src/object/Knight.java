package object;

import board.Board;
/**
 * 
 * @author Manpreet Singh
 * @author Daniel Egladyous
 */




public class Knight extends Piece {
	
	/**
	 * Constructor for Knight Class
	 * @param x Current X coordinate
	 * @param y Current Y coordinate
	 * @param team True is White, False is Black
	 */
	public Knight(int x, int y, boolean team) {
		this.x = x;
		this.y = y;
		
		this.team = team;
	}

	@Override
	public boolean isPathClear(Piece[][] board, int x, int y) {
		int oldx = this.x;
		int oldy = this.y;
		
		int deltax = Math.abs(oldx - x);
		int deltay = Math.abs(oldy - y);
		
		if ((deltax == 2 && deltay == 1) || (deltax == 1 && deltay == 2)) {
			return true;
		}
		
		return false;
	}
	

}
